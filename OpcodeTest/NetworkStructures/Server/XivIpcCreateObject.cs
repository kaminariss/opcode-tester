﻿using NextUIPlugin.NetworkStructures.Common;

namespace NextUIPlugin.NetworkStructures.Server {
	public struct XivIpcCreateObject {
		public byte index;
		public byte kind;
		public byte flag;
		public byte padding1;
		public uint baseId;
		public uint entityId;
		public uint layoutId;
		public uint contentId;
		public uint ownerId;
		public uint bindLayoutId;
		public float scale;
		public ushort sharedGroupTimelineState;
		public ushort dir;
		public ushort FATE;
		public byte permissionInvisibility;
		public byte args;
		public uint args2;
		public uint args3;
		public Position position;
		// Common::FFXIVARR_POSITION3 Pos;
	}
}