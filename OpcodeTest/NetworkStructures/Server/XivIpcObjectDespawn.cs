﻿namespace NextUIPlugin.NetworkStructures.Server {
	public struct XivIpcObjectDespawn {
		public byte spawnId;
		public byte unknown1;
		public ushort unknown2;
		public uint actorId;
	}
}