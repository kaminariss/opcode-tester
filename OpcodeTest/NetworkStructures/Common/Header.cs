﻿using System;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace NextUIPlugin.NetworkStructures.Common {
	[Serializable]
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Header {
		public uint messageLength;
		public uint sourceId;
		public uint loginUserId;

		public uint unknown1;

		public ushort unknown2;
		public ushort messageType;

		public uint unknown3;
		public uint timestamp;
		//

		public uint unknown4;
		
		// /** Unknown data, no actual use has been determined */
		// public ulong unknown_0;
		// public ulong unknown_8;
		// /** Represents the number of milliseconds since epoch that the packet was sent. */
		// public ulong timestamp;
		// /** The size of the packet header and its payload */
		// public uint size;
		// /** The type of this connection - 1 zone, 2 chat*/
		// public ushort connectionType;
		// /** The number of packet segments that follow. */
		// public ushort count;
		// public byte unknown_20;
		// /** Indicates if the data segments of this packet are compressed. */
		// public byte isCompressed;
		// public uint unknown_24;
		
		// /** The size of the segment header and its data. */
		// public uint size;
		// /** The session ID this segment describes. */
		// public uint sourceId;
		// /** The session ID this packet is being delivered to. */
		// public uint targetId;
		// /** The segment type. (1, 2, 3, 7, 8, 9, 10) */
		// public ushort type;
		// public ushort padding;
	}
}