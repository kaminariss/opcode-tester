﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Lumina;
using Lumina.Excel;
using Newtonsoft.Json;
using NextUIPlugin.NetworkStructures;
using NextUIPlugin.NetworkStructures.Server;
using Action = Lumina.Excel.GeneratedSheets.Action;
using Status = Lumina.Excel.GeneratedSheets.Status;
using FFXIVClientStructs.FFXIV.Client.Game;
using Lumina.Extensions;
using Lumina.Text;
using Lumina.Text.Payloads;
using NextUIPlugin.NetworkStructures.Common;

namespace OpcodeTest {
	class Program {
		protected static GameData lumina = new GameData(
			@"E:\Gry\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\game\sqpack"
		);

		static string opcodeDir =
			@"C:\Users\Kaminari\AppData\Roaming\XIVLauncher\pluginConfigs\NextUIPlugin\NetworkLogs";

		public static int GetHexVal(char hex) {
			var val = (int)hex;
			return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
		}

		public static byte[] StringToByteArray(string hex) {
			if (hex.Length % 2 == 1)
				throw new Exception("The binary key cannot have an odd number of digits");

			var arr = new byte[hex.Length >> 1];

			for (var i = 0; i < hex.Length >> 1; ++i) {
				arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
			}

			return arr;
		}

		public static byte[] StringToByteArray2(string hex) {
			return Enumerable.Range(0, hex.Length)
				.Where(x => x % 2 == 0)
				.Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
				.ToArray();
		}

		public static void LoopFiles<T>() {
			var files = Directory.GetFiles(opcodeDir);

			foreach (var file in files) {
				var dataRaw = File.ReadAllText(file);
				Console.WriteLine(file);

				var filtered = FilterData(dataRaw);

				if (filtered.Length < 6) {
					continue;
				}

				var (str, _) = ParseToStruct<T>(filtered[5]);
				Console.WriteLine(JsonConvert.SerializeObject(str, Formatting.Indented));
				Console.ReadLine();
			}
		}

		public static void Main(string[] args) {
			// DumpStatuses();
			// DumpActions();
			// To find matching struct
			// LoopFiles<XivIpcObjectDespawn>();

			// To find if its 100% correct
			// TryParse<XivIpcCreateObject>("Down_0x" + ((ushort)ServerZoneIpcType.CreateObject).ToString("X4") + ".txt");
			TryParse<XivIpcObjectDespawn>("Down_0x"+ ((ushort)ServerZoneIpcType.ObjectDespawn).ToString("X4") + ".txt");
			// TryParse<XivIpcChatToChannel>("Down_0x"+ ((ushort)ServerZoneIpcType.ChatToChannel).ToString("X4") + ".txt");
			// TryParse<XivIpcEffectResult>("Down_0x"+ ((ushort)ServerZoneIpcType.EffectResult).ToString("X4") + ".txt");
			//ConvertCasts();
			//TryParse<XivIpcActionEffect1>("Down_0x"+ ((ushort)ServerZoneIpcType.ActionEffect1).ToString("X4") + ".txt");
			// TryParse<XivIpcActionEffect8>("Down_0x"+ ((ushort)ServerZoneIpcType.ActionEffect8).ToString("X4") + ".txt");
			// TryParse<XivIpcActionEffect16>("Down_0x"+ ((ushort)ServerZoneIpcType.ActionEffect16).ToString("X4") + ".txt");
			// NO DUMP FOUND
			// TryParse<XivIpcActionEffect24>("Down_0x"+ ((ushort)ServerZoneIpcType.ActionEffect24).ToString("X4") + ".txt");
			// TryParse<XivIpcActionEffect32>("Down_0x"+ ((ushort)ServerZoneIpcType.ActionEffect32).ToString("X4") + ".txt");
			// NO DUMP FOUND END
			// TryParse<XivIpcActorControl>("Down_0x"+ ((ushort)ServerZoneIpcType.ActorControl).ToString("X4") + ".txt");
			// TryParse<XivIpcUpdateHpMpTp>("Down_0x"+ ((ushort)ServerZoneIpcType.UpdateHpMpTp).ToString("X4") + ".txt");
			// TryParse<XivIpcPlayerSpawn>("Down_0x"+ ((ushort)ServerZoneIpcType.PlayerSpawn).ToString("X4") + ".txt");
			//TryParse<XivIpcNpcSpawn>("Down_0x"+ ((ushort)ServerZoneIpcType.NpcSpawn2).ToString("X4") + ".txt");
			// TryParse<XivIpcActorMove>("Down_0x"+ ((ushort)ServerZoneIpcType.ActorMove).ToString("X4") + ".txt");
			// TryParse<XivIpcActorSetPos>("Down_0x"+ ((ushort)ServerZoneIpcType.ActorSetPos).ToString("X4") + ".txt");
			// TryParse<XivIpcActorGauge>("Down_0x"+ ((ushort)ServerZoneIpcType.ActorGauge).ToString("X4") + ".txt");
			// TryParse<XivIpcPlaceFieldMarkerPreset>(
			// 	"Down_0x"+ ((ushort)ServerZoneIpcType.PlaceFieldMarkerPreset).ToString("X4") + ".txt"
			// );
			// TryParse<XivIpcPlaceFieldMarker>(
			// 	"Down_0x"+ ((ushort)ServerZoneIpcType.PlaceFieldMarker).ToString("X4") + ".txt"
			// );

			// UP

			// TryParse<XIVIpcChatHandler>("Up_0x"+ ((ushort)ClientZoneIpcType.ChatHandler).ToString("X4") + ".txt");
		}

		public static dynamic ToDynamic(object obj) {
			IDictionary<string, object> expando = new ExpandoObject();
			var type = obj.GetType();

			foreach (var fieldInfo in type.GetFields()) {
				expando.Add(fieldInfo.Name, fieldInfo.GetValue(obj));
			}

			foreach (var propertyInfo in type.GetProperties()) {
				expando.Add(propertyInfo.Name, propertyInfo.GetValue(obj));
			}

			return (ExpandoObject)expando;
		}

		// public static void ConvertCasts() {
		// 	var ou = Parse<XivIpcActorCast>("Down_0x" + ((ushort)ServerZoneIpcType.ActorCast).ToString("X4") + ".txt");
		// 	foreach (var (cast, header) in ou) {
		// 		var castObj = ToDynamic(cast);
		//
		// 		var action = lumina.GetExcelSheet<Action>()?.GetRow(cast.actionId);
		// 		var actionTypeName = (ActionType)cast.actionType;
		//
		// 		Console.WriteLine(JsonConvert.SerializeObject(header, Formatting.Indented));
		// 		castObj.sourceId = header.sourceId;
		// 		// castObj.timestamp = header.timestamp;
		// 		castObj.actionTypeName = actionTypeName.ToString();
		// 		castObj.actionName = action?.Name.ToString();
		//
		// 		Console.WriteLine(JsonConvert.SerializeObject(castObj, Formatting.Indented));
		// 		Console.ReadLine();
		// 	}
		// }

		public static void ConvertCasts() {
			// var ou = Parse<XivIpcActionEffect1>(
			// 	"Down_0x" + ((ushort)ServerZoneIpcType.ActionEffect1).ToString("X4") + ".txt"
			// 	);
			// foreach (var (cast, header) in ou) {
			// 	var castObj = ToDynamic(cast);
			//
			// 	var action = lumina.GetExcelSheet<Action>()?.GetRow(cast.actionId);
			// 	var actionTypeName = (ActionType)cast.actionType;
			//
			// 	Console.WriteLine(JsonConvert.SerializeObject(header, Formatting.Indented));
			// 	castObj.sourceId = header.sourceId;
			// 	// castObj.timestamp = header.timestamp;
			// 	castObj.actionTypeName = actionTypeName.ToString();
			// 	castObj.actionName = action?.Name.ToString();
			//
			// 	Console.WriteLine(JsonConvert.SerializeObject(castObj, Formatting.Indented));
			// 	Console.ReadLine();
			// }
		}

		public static List<Tuple<T, Header>> Parse<T>(string fname) {
			var dataRaw = File.ReadAllText(
				Path.Combine(opcodeDir, fname)
			);

			var filtered = FilterData(dataRaw);
			var output = new List<Tuple<T, Header>>();
			foreach (var line in filtered) {
				output.Add(ParseToStruct<T>(line));
			}

			return output;
		}

		public static void TryParse<T>(string fname) {
			var filtered = Parse<T>(fname);

			foreach (var (obj, header) in filtered) {
				var serialized = JsonConvert.SerializeObject(obj, Formatting.Indented);
				File.AppendAllText("out.txt", serialized + Environment.NewLine);
				Console.WriteLine(serialized);
				Console.ReadLine();
			}
		}

		public static Image ImageFromRawArray(byte[] arr, int width, int height, PixelFormat pixelFormat) {
			var output = new Bitmap(width, height, pixelFormat);
			var rect = new Rectangle(0, 0, width, height);
			var bmpData = output.LockBits(rect, ImageLockMode.ReadWrite, output.PixelFormat);

			// Row-by-row copy
			var arrRowLength = width * Image.GetPixelFormatSize(output.PixelFormat) / 8;
			var ptr = bmpData.Scan0;
			for (var i = 0; i < height; i++) {
				Marshal.Copy(arr, i * arrRowLength, ptr, arrRowLength);
				ptr += bmpData.Stride;
			}

			output.UnlockBits(bmpData);
			return output;
		}

		public static string ToDalamudString(SeString str) {
			return str.ToString().Length == 0
				? ""
				: str.Payloads
					.Where(payload => payload is TextPayload).Cast<TextPayload>()
					.Select(text => text.RawString)
					.Aggregate(string.Concat);
		}

		public static void DumpActions() {
			var actions = lumina.GetExcelSheet<Action>();
			var all = new List<object>();
			
			var iconBaseDir = @"A:\Projects\Kaminaris\ffxiv\OpcodeTest\Icons";

			var obsolete = new List<uint>();
			foreach (var a in actions) {
				if (a.Name.ToString().Length == 0) {
					continue;
				}

				if (a.ClassJob.Value == null && !obsolete.Contains(a.Icon)) {
					obsolete.Add(a.Icon);
				}
				var data = new {
					id = a.RowId,
					name = a.Name.ToString(),
					iconId = a.Icon,
					category = a.ActionCategory.Value?.RowId,
					castTime = a.Cast100ms / 10f,
					cooldown = a.Recast100ms / 10f,
				};
				all.Add(data);
				var iconPath = Path.Combine(iconBaseDir, a.Icon + ".png");
				if (!File.Exists(iconPath)) {
					var texIcon = lumina.GetHqIcon(a.Icon);
					if (texIcon != null) {
						var img = ImageFromRawArray(
							texIcon.ImageData,
							texIcon.Header.Width,
							texIcon.Header.Height,
							PixelFormat.Format32bppArgb
						);
						img.Save(
							iconPath,
							ImageFormat.Png
						);
					}
				}

				Console.WriteLine(JsonConvert.SerializeObject(data));
			}

			Console.WriteLine("obsolete: " + JsonConvert.SerializeObject(obsolete));
			var conv = JsonConvert.SerializeObject(all, Formatting.Indented)
					.Replace("\"id\":", "id:")
					.Replace("\"name\":", "name:")
					.Replace("\"iconId\":", "iconId:")
					.Replace("\"description\":", "description:")
					.Replace("\"category\":", "category:")
					.Replace("\"castTime\":", "castTime:")
					.Replace("\"cooldown\":", "cooldown:")
				// .Replace("'s", "\\'")
				// .Replace("\"", "'")
				;

			File.WriteAllText(
				@"A:\Projects\Kaminaris\ffxiv\OpcodeTest\actions.ts",
				"export const actions = " + conv + ";"
			);
		}

		public static void DumpStatuses() {
			var statuses = lumina.GetExcelSheet<Status>();
			var all = new List<object>();
			var iconBaseDir = @"A:\Projects\Kaminaris\ffxiv\OpcodeTest\Icons";
			foreach (var status in statuses) {
				if (status.Name.ToString().Length == 0) {
					continue;
				}

				var data = new {
					id = status.RowId,
					name = status.Name.ToString(),
					iconId = status.Icon,
					description = ToDalamudString(status.Description),
					category = status.Category,
					isPermanent = status.IsPermanent,
					maxStacks = status.MaxStacks,
					partyListPriority = status.PartyListPriority
				};
				all.Add(data);
				
				var iconPath = Path.Combine(iconBaseDir, status.Icon + ".png");
				if (!File.Exists(iconPath)) {
					var texIcon = lumina.GetHqIcon(status.Icon);
					if (texIcon != null) {
						var img = ImageFromRawArray(
							texIcon.ImageData,
							texIcon.Header.Width,
							texIcon.Header.Height,
							PixelFormat.Format32bppArgb
						);
						img.Save(
							iconPath,
							ImageFormat.Png
						);
					}
				}

				Console.WriteLine(JsonConvert.SerializeObject(data));
			}

			var conv = JsonConvert.SerializeObject(all, Formatting.Indented)
					.Replace("\"id\":", "id:")
					.Replace("\"name\":", "name:")
					.Replace("\"iconId\":", "iconId:")
					.Replace("\"description\":", "description:")
					.Replace("\"category\":", "category:")
					.Replace("\"isPermanent\":", "isPermanent:")
					.Replace("\"maxStacks\":", "maxStacks:")
					.Replace("\"partyListPriority\":", "partyListPriority:")
				// .Replace("'s", "\\'")
				// .Replace("\"", "'")
				;

			File.WriteAllText(
				@"A:\Projects\Kaminaris\ffxiv\OpcodeTest\statuses.ts",
				"export const statuses = " + conv + ";"
			);
		}

		static private Tuple<T, Header> ParseToStruct<T>(string firstLine) {
			var data = StringToByteArray(firstLine);

			// Console.WriteLine(firstLine.Substring(2*0x20));
			var headerPtr = Marshal.UnsafeAddrOfPinnedArrayElement(data, 0);
			var dataPtr = Marshal.UnsafeAddrOfPinnedArrayElement(data, 0x20);
			var header = Marshal.PtrToStructure<Header>(headerPtr);
			var str = Marshal.PtrToStructure<T>(dataPtr);

			return Tuple.Create(str, header);
		}

		static private string[] FilterData(string dataRaw) {
			var filtered = dataRaw
					.Split('\n')
					.Where(x => !string.IsNullOrWhiteSpace(x))
					.Where(x => x.Length > 100)
					.Select(l => l.Trim())
					.ToArray()
				;
			return filtered;
		}
	}
}